<?php
// $Id: $

/**
 * Source for localization files.
 */
#define('CONTENTO_SERVER_URL', 'http://ftp.drupal.org/files/translations');
define('CONTENTO_SERVER_URL', 'http://localize.dinamico.si/sites/localize.dinamico.si/files/translations');


/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function contento_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  
  // TODO: Only for development. Remove on production site!!!
  $form['site_information']['site_mail']['#default_value'] = 'info@site.com';
  $form['admin_account']['account']['name']['#default_value'] = 'Superadmin';
  
  #print_r($form);
}


/**
 * Implement hook_install_tasks_alter().
 *
 * Perform actions to set up the site for this profile.
 */
function contento_install_tasks_alter(&$tasks, $install_state) {
  // Remove core steps for translation imports.
  unset($tasks['install_import_locales']);
  unset($tasks['install_import_locales_remaining']);
}

/**
 * Implement hook_install_tasks().
 */
function contento_install_tasks($install_state) {
  // Determine whether translation import tasks will need to be performed.
  $needs_translations = count($install_state['locales']) > 1 && !empty($install_state['parameters']['locale']) && $install_state['parameters']['locale'] != 'en';

  return array(
    'contento_import_translation' => array(
      'display_name' => st('Set up translations'),
      'display' => $needs_translations,
      'run' => $needs_translations ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
    ),
    'contento_import_translation_remaining' => array(
      'display_name' => st('Set up translations'),
      'display' => $needs_translations,
      'type' => 'batch',
      'run' => $needs_translations ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
    ),    
  );
}

/**
 * Installation step callback.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 */
function contento_import_translation(&$install_state) {
  include_once DRUPAL_ROOT . '/includes/locale.inc';
  $install_locale = $install_state['parameters']['locale'];

  // Enable installation language as default site language.
  locale_add_language($install_locale, NULL, NULL, NULL, '', NULL, 1, TRUE);

  // @todo: inelegant method to overcome that -dev versions might be running.
  // Just fall back on alpha6 for now, the latest alpha version.
  
  $version = '7.9'; // str_replace('-dev', '', VERSION);

  // @todo: this will also time out / reach memory limits on certain
  // underpowered PHP installations. We should integrate a batch process for
  // importing the downloaded file (existing issue with some code available).
  $po_url = CONTENTO_SERVER_URL .'/7.x/drupal/drupal-'. $version .'.'. $install_locale .'.po';
  $result = drupal_http_request($po_url);

  if ($result->code == 200) {
    include_once 'includes/locale.inc';
    $tempfile = tempnam(file_directory_temp(), 'l10n-install');
    file_put_contents($tempfile, $result->data);

    $file = (object) array('uri' => $tempfile, 'filename' => basename($tempfile));
    if (_locale_import_po($file, $install_locale, LOCALE_IMPORT_KEEP, 'default')) {
      drupal_set_message(t('Downloaded translation import of %file successful.', array('%file' => basename($po_url))));
    }
    else {
      drupal_set_message(t('Downloaded translation import failed.'), 'error');
    }
    return;
  }
  else {
    drupal_set_message(t('Unable to download translations from @url.', array('@url' => $po_url)), 'error');
  }
}

function contento_import_translation_remaining(&$install_state) {
  include_once DRUPAL_ROOT . '/includes/locale.inc';
  // Collect files to import for this language. Skip components already covered
  // in the initial batch set.
  $install_locale = $install_state['parameters']['locale'];
  $batch = locale_batch_by_language($install_locale, NULL, array());
  // Remove temporary variable.
  #variable_del('install_locale_batch_components');
  #return $batch;
}
