core = 7.x
api = 2

; ACCESS:

projects[block_access][type] = "module"
projects[block_access][version] = "1.5"
projects[block_access][subdir] = "contrib"

projects[field_permissions][type] = "module"
projects[field_permissions][version] = "1.0-beta2"
projects[field_permissions][subdir] = "contrib"

projects[field_ui_permissions][type] = "module"
projects[field_ui_permissions][version] = "1.0-beta1"
projects[field_ui_permissions][subdir] = "contrib"

projects[menu_admin_per_menu][type] = "module"
projects[menu_admin_per_menu][version] = "1.0"
projects[menu_admin_per_menu][subdir] = "contrib"

projects[taxonomy_access_fix][type] = "module"
projects[taxonomy_access_fix][version] = "2.1"
projects[taxonomy_access_fix][subdir] = "contrib"


; ADMIN:

projects[admin_menu][type] = "module"
projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[coffee][type] = "module"
projects[coffee][version] = "2.2"
projects[coffee][subdir] = "contrib"

projects[contextual_flyout_links][type] = "module"
projects[contextual_flyout_links][version] = "1.2"
projects[contextual_flyout_links][subdir] = "contrib"
projects[contextual_flyout_links][patch][] = "https://drupal.org/files/contextual_flyout_links-remove_div_selector-1479424-1479424.patch"

	;projects[clean_module_list][type] = "module"
	;projects[clean_module_list][version] = "1.0"
	;projects[clean_module_list][subdir] = "contrib"

projects[instantfilter][type] = "module"
projects[instantfilter][version] = "1.x-dev"
projects[instantfilter][subdir] = "contrib"

projects[logintoboggan][type] = "module"
projects[logintoboggan][version] = "1.4"
projects[logintoboggan][subdir] = "contrib"

projects[module_dep_toggle][type] = "module"
projects[module_dep_toggle][version] = "1.0"
projects[module_dep_toggle][subdir] = "contrib"

projects[module_filter][type] = "module"
projects[module_filter][version] = "2.0-alpha2"
projects[module_filter][subdir] = "contrib"

projects[mpac][type] = "module"
projects[mpac][version] = "1.2"
projects[mpac][subdir] = "contrib"

projects[simplified_menu_admin][type] = "module"
projects[simplified_menu_admin][version] = "1.0-beta2"
projects[simplified_menu_admin][subdir] = "contrib"

projects[workbench][type] = "module"
projects[workbench][version] = "1.2"
projects[workbench][subdir] = "contrib"

projects[workbench_access][type] = "module"
projects[workbench_access][version] = "1.2"
projects[workbench_access][subdir] = "contrib"

projects[workbench_media][type] = "module"
projects[workbench_media][version] = "1.1"
projects[workbench_media][subdir] = "contrib"

projects[workbench_moderation][type] = "module"
projects[workbench_moderation][version] = "1.3"
projects[workbench_moderation][subdir] = "contrib"


; FEATURES:

projects[contento_features][type] = "module"
projects[contento_features][download][type] = "file"
projects[contento_features][download][url] = "https://bitbucket.org/dinamico/contento_features/get/master.zip"


; FIELDS:

projects[acdx_references][type] = "module"
projects[acdx_references][version] = "1.x-dev"
projects[acdx_references][subdir] = "contrib"

projects[addressfield][type] = "module"
projects[addressfield][version] = "1.0-beta5"
projects[addressfield][subdir] = "contrib"

projects[autocomplete_deluxe][type] = "module"
projects[autocomplete_deluxe][version] = "2.0-beta3"
projects[autocomplete_deluxe][subdir] = "contrib"

projects[content_taxonomy][type] = "module"
projects[content_taxonomy][version] = "1.0-beta2"
projects[content_taxonomy][subdir] = "contrib"

projects[countries][version] = 2.1
projects[countries][subdir] = "contrib"

projects[date][type] = "module"
projects[date][version] = "2.7"
projects[date][subdir] = "contrib"

projects[email][type] = "module"
projects[email][version] = "1.3"
projects[email][subdir] = "contrib"

projects[entityreference][type] = "module"
projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = "contrib"

projects[fences][type] = "module"
projects[fences][version] = "1.x-dev"
projects[fences][subdir] = "contrib"
projects[fences][patch][] = "https://bitbucket.org/dinamico/contento/downloads/field_for_wrapper_css_class-1679684-8.patch"
	;projects[fences][patch][] = https://drupal.org/files/field_for_wrapper_css_class-1679684-3.patch

projects[field_collection][type] = "module"
projects[field_collection][version] = "1.0-beta7"
projects[field_collection][subdir] = "contrib"

projects[field_group][type] = "module"
projects[field_group][version] = "1.3"
projects[field_group][subdir] = "contrib"

projects[link][type] = "module"
projects[link][version] = "1.2"
projects[link][subdir] = "contrib"

projects[ref_field][type] = "module"
projects[ref_field][version] = "2.0-alpha1"
projects[ref_field][subdir] = "contrib"

projects[references][type] = "module"
projects[references][version] = "2.1"
projects[references][subdir] = "contrib"

projects[smart_trim][type] = "module"
projects[smart_trim][version] = "1.4"
projects[smart_trim][subdir] = "contrib"

projects[text_noderef][type] = "module"
projects[text_noderef][version] = "1.x-dev"
projects[text_noderef][subdir] = "contrib"

projects[title][type] = "module"
projects[title][version] = "1.x-dev"
projects[title][subdir] = "contrib"
	;projects[title][patch][] = "https://www.drupal.org/files/title-formatters-1062814-10.patch"

; MEDIA:

projects[bxslider][type] = "module"
projects[bxslider][version] = "1.x-dev"
projects[bxslider][subdir] = "contrib"

projects[bxslider_views_slideshow][type] = "module"
projects[bxslider_views_slideshow][version] = "1.50"
projects[bxslider_views_slideshow][subdir] = "contrib"

projects[colorbox][type] = "module"
projects[colorbox][version] = "2.7"
projects[colorbox][subdir] = "contrib"

projects[file_entity][type] = "module"
projects[file_entity][version] = "2.0-alpha3"
projects[file_entity][subdir] = "contrib"

	;projects[flexslider][type] = "module"
	;projects[flexslider][version] = "2.0-alpha3"
	;projects[flexslider][subdir] = "contrib"

	;projects[flexslider_views_slideshow][type] = "module"
	;projects[flexslider_views_slideshow][version] = "2.x-dev"
	;projects[flexslider_views_slideshow][subdir] = "contrib"

	;projects[filefield_sources][type] = "module"
	;projects[filefield_sources][version] = ""
	;projects[filefield_sources][subdir] = "contrib"

	;projects[filefield_sources_plupload][type] = "module"
	;projects[filefield_sources_plupload][version] = ""
	;projects[filefield_sources_plupload][subdir] = "contrib"

projects[imagecache_actions][type] = "module"
projects[imagecache_actions][version] = "1.x-dev"
projects[imagecache_actions][subdir] = "contrib"

projects[manualcrop][type] = "module"
projects[manualcrop][version] = "1.4"
projects[manualcrop][subdir] = "contrib"

projects[media][type] = "module"
projects[media][version] = "2.x-dev"
projects[media][subdir] = "contrib"
	;projects[media][patch][] = "https://drupal.org/files/media.media-browser.1956620-10.patch"
	;projects[media][patch][] = "https://drupal.org/files/media.code_.1701914-2.patch"
	;projects[media][patch][] = "https://drupal.org/files/media-857362-selection-bookmarking.patch"

projects[media_browser_plus][type] = "module"
projects[media_browser_plus][version] = "3.x-dev"
projects[media_browser_plus][subdir] = "contrib"

projects[media_vimeo][type] = "module"
projects[media_vimeo][version] = "1.x-dev"
projects[media_vimeo][subdir] = "contrib"

projects[media_youtube][type] = "module"
projects[media_youtube][version] = "2.x-dev"
projects[media_youtube][subdir] = "contrib"

  ;projects[nested_image_style][type] = "module"
  ;projects[nested_image_style][version] = "1.0"
  ;projects[nested_image_style][subdir] = "contrib"

projects[picture][type] = "module"
projects[picture][version] = "1.2"
projects[picture][subdir] = "contrib"

projects[plupload][type] = "module"
projects[plupload][version] = "1.6"
projects[plupload][subdir] = "contrib"


; OTHER:

projects[backports][type] = "module"
projects[backports][version] = "1.0-alpha1"
projects[backports][subdir] = "contrib"

projects[ckeditor][type] = "module"
projects[ckeditor][version] = "1.15"
projects[ckeditor][subdir] = "contrib"

projects[clean_markup][type] = "module"
projects[clean_markup][version] = "2.7"
projects[clean_markup][subdir] = "contrib"

projects[currency][type] = "module"
projects[currency][version] = "2.x-dev"
projects[currency][subdir] = "contrib"

projects[elements][type] = "module"
projects[elements][version] = "1.4"
projects[elements][subdir] = "contrib"

projects[entity][type] = "module"
projects[entity][version] = "1.5"
projects[entity][subdir] = "contrib"

projects[inline_entity_form][version] = 1.5
projects[inline_entity_form][subdir] = "contrib"

projects[inline_conditions][version] = 1.x-dev
projects[inline_conditions][subdir] = "contrib"

projects[jquery_update][type] = "module"
projects[jquery_update][version] = "2.4"
projects[jquery_update][subdir] = "contrib"

projects[json2][type] = "module"
projects[json2][version] = "1.x-dev"
projects[json2][subdir] = "contrib"

projects[libraries][type] = "module"
projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[message][version] = 1.9
projects[message][subdir] = "contrib"

projects[message_notify][version] = 2.5
projects[message_notify][subdir] = "contrib"

projects[multiform][type] = "module"
projects[multiform][version] = "1.1"
projects[multiform][subdir] = "contrib"

projects[physical][version] = 1.x-dev
projects[physical][subdir] = "contrib"

projects[shortcode][type] = "module"
projects[shortcode][version] = "2.1"
projects[shortcode][subdir] = "contrib"

projects[token][type] = "module"
projects[token][version] = "1.x-dev"
projects[token][subdir] = "contrib"

projects[transliteration][type] = "module"
projects[transliteration][version] = "3.x-dev"
projects[transliteration][subdir] = "contrib"

projects[variable][type] = "module"
projects[variable][version] = "2.5"
projects[variable][subdir] = "contrib"


; PANELS:

; Field API Pane Editor (FAPE)
; This module adds a contextual link to the Entity Field panel pane which
; allows that field to be edited without having to visit the entity's edit page. 
; This field editor will open in an overlay if overlay.module is enabled.
; This only affects the entity pane for a single field.
projects[fape][type] = "module"
projects[fape][version] = "1.x-dev"
projects[fape][subdir] = "contrib"

projects[fieldable_panels_panes][type] = "module"
projects[fieldable_panels_panes][version] = "1.x-dev"
projects[fieldable_panels_panes][subdir] = "contrib"

projects[gridbuilder][type] = "module"
projects[gridbuilder][version] = "1.x-dev"
projects[gridbuilder][subdir] = "contrib"

projects[layout][type] = "module"
projects[layout][version] = "1.x-dev"
projects[layout][subdir] = "contrib"

projects[panelizer][type] = "module"
projects[panelizer][version] = "3.x-dev"
projects[panelizer][subdir] = "contrib"

projects[panels][type] = "module"
projects[panels][version] = "3.4"
projects[panels][subdir] = "contrib"

projects[panels_everywhere][type] = "module"
projects[panels_everywhere][version] = "1.x-dev"
projects[panels_everywhere][subdir] = "contrib"

projects[panels_extra_styles][type] = "module"
projects[panels_extra_styles][version] = "1.x-dev"
projects[panels_extra_styles][subdir] = "contrib"

projects[panopoly_magic][type] = "module"
projects[panopoly_magic][version] = "1.1"
projects[panopoly_magic][subdir] = "contrib"

projects[pm_existing_pages][type] = "module"
projects[pm_existing_pages][version] = "1.x-dev"
projects[pm_existing_pages][subdir] = "contrib"

projects[semantic_panels][type] = "module"
projects[semantic_panels][version] = "1.x-dev"
projects[semantic_panels][subdir] = "contrib"

; PERFORMANCE:

projects[cdn][type] = "module"
projects[cdn][version] = "2.6"
projects[cdn][subdir] = "contrib"

projects[entitycache][type] = "module"
projects[entitycache][version] = "1.2"
projects[entitycache][subdir] = "contrib"

projects[advagg][type] = "module"
projects[advagg][version] = "2.6"
projects[advagg][subdir] = "contrib"

projects[fast_404][type] = "module"
projects[fast_404][version] = "1.3"
projects[fast_404][subdir] = "contrib"


; SEARCH:

projects[facetapi][type] = "module"
projects[facetapi][version] = "1.3"
projects[facetapi][subdir] = "contrib"

projects[search_api][type] = "module"
projects[search_api][version] = "1.12"
projects[search_api][subdir] = "contrib"

projects[search_api_db][type] = "module"
projects[search_api_db][version] = "1.3"
projects[search_api_db][subdir] = "contrib"

projects[search_api_ranges][type] = "module"
projects[search_api_ranges][version] = "1.5"
projects[search_api_ranges][subdir] = "contrib"

projects[search_api_sorts][type] = "module"
projects[search_api_sorts][version] = "1.x-dev"
projects[search_api_sorts][subdir] = "contrib"


; SEO:

projects[globalredirect][type] = "module"
projects[globalredirect][version] = "1.x-dev"
projects[globalredirect][subdir] = "contrib"

projects[google_analytics][type] = "module"
projects[google_analytics][version] = "1.x-dev"
projects[google_analytics][subdir] = "contrib"

projects[metatag][type] = "module"
projects[metatag][version] = "1.0-rc1"
projects[metatag][subdir] = "contrib"

projects[redirect][type] = "module"
projects[redirect][version] = "1.x-dev"
projects[redirect][subdir] = "contrib"

projects[seo_checklist][type] = "module"
projects[seo_checklist][version] = "4.1"
projects[seo_checklist][subdir] = "contrib"

projects[seo_ui][type] = "module"
projects[seo_ui][version] = "1.x-dev"
projects[seo_ui][subdir] = "contrib"
projects[seo_ui][patch][] = "https://drupal.org/files/1394342_fix-undefined-indices_15.patch"

projects[site_verify][type] = "module"
projects[site_verify][version] = "1.1"
projects[site_verify][subdir] = "contrib"

projects[xmlsitemap][type] = "module"
projects[xmlsitemap][version] = "2.x-dev"
projects[xmlsitemap][subdir] = "contrib"


; TOOLS:

projects[backup_migrate][type] = "module"
projects[backup_migrate][version] = "3.x-dev"
projects[backup_migrate][subdir] = "contrib"

projects[breakpoints][type] = "module"
projects[breakpoints][version] = "1.1"
projects[breakpoints][subdir] = "contrib"

projects[ctools][type] = "module"
projects[ctools][version] = "1.4"
projects[ctools][subdir] = "contrib"
; Add option for Page Site Name pane to link to the homepage [https://www.drupal.org/node/2247071]
projects[ctools][patch][] = "https://www.drupal.org/files/issues/ctools-n2247071-2.patch"

projects[defaultconfig][type] = "module"
projects[defaultconfig][version] = "1.0-alpha9"
projects[defaultconfig][subdir] = "contrib"

projects[delete_orphaned_terms][type] = "module"
projects[delete_orphaned_terms][version] = "2.0-beta2"
projects[delete_orphaned_terms][subdir] = "contrib"

projects[devel][type] = "module"
projects[devel][version] = "1.x-dev"
projects[devel][subdir] = "contrib"

projects[diff][type] = "module"
projects[diff][version] = "3.x-dev"
projects[diff][subdir] = "contrib"

projects[features][type] = "module"
projects[features][version] = "2.0"
projects[features][subdir] = "contrib"

projects[features_override][type] = "module"
projects[features_override][version] = "2.0-rc1"
projects[features_override][subdir] = "contrib"

projects[feeds][type] = "module"
projects[feeds][version] = "2.x-dev"
projects[feeds][subdir] = "contrib"

projects[feeds_crawler][type] = "module"
projects[feeds_crawler][version] = "2.x-dev"
projects[feeds_crawler][subdir] = "contrib"

projects[feeds_tamper][type] = "module"
projects[feeds_tamper][version] = "1.0-beta5"
projects[feeds_tamper][subdir] = "contrib"

projects[feeds_xpathparser][type] = "module"
projects[feeds_xpathparser][version] = "1.x-dev"
projects[feeds_xpathparser][subdir] = "contrib"

projects[job_scheduler][type] = "module"
projects[job_scheduler][version] = "2.x-dev"
projects[job_scheduler][subdir] = "contrib"

projects[pathauto][type] = "module"
projects[pathauto][version] = "1.x-dev"
projects[pathauto][subdir] = "contrib"

; Get working Drupal gardens version of module. Download from bitbucket.
projects[pathauto_live_preview][type] = "module"
projects[pathauto_live_preview][subdir] = "contrib"
projects[pathauto_live_preview][download][type] = "file"
projects[pathauto_live_preview][download][url] = "https://bitbucket.org/dinamico/drupal/downloads/drupalgardens_pathauto_live_preview.tar.gz"
  ;projects[pathauto_live_preview][download][type] = "git"
  ;projects[pathauto_live_preview][download][url] = http://git.drupal.org/sandbox/pyrello/1600922.git
  ;projects[pathauto_live_preview][download][revision] = 32e0368b3a5c03600fb355adae6fa2415005478f
  ;projects[pathauto_live_preview][patch][] = "https://bitbucket.org/dinamico/contento/downloads/pathauto_live_preview-update-to-drupalgardens-version.patch"

projects[pathauto_persist][type] = "module"
projects[pathauto_persist][version] = "1.3"
projects[pathauto_persist][subdir] = "contrib"

  ;projects[plus][type] = "module"
  ;projects[plus][subdir] = "contrib"
  ;projects[plus][download][type] = "file"
  ;projects[plus][download][url] = "https://bitbucket.org/dinamico/plus/get/master.zip"

	;projects[plus][type] = "module"
	;projects[plus][subdir] = "contrib"
	;projects[plus][download][type] = "git"
		;projects[plus][download][branch] = "7.x-1.0-dev"
	;projects[plus][download][url] = "https://dinamico@bitbucket.org/dinamico/plus.git"

projects[profile_status_check][type] = "module"
projects[profile_status_check][version] = "1.x-dev"
projects[profile_status_check][subdir] = "contrib"

projects[responsive_preview][type] = "module"
projects[responsive_preview][version] = "1.1"
projects[responsive_preview][subdir] = "contrib"

projects[rules][type] = "module"
projects[rules][version] = "2.7"
projects[rules][subdir] = "contrib"

projects[scheduler][type] = "module"
projects[scheduler][version] = "1.2"
projects[scheduler][subdir] = "contrib"

projects[strongarm][type] = "module"
projects[strongarm][version] = "2.x-dev"
projects[strongarm][subdir] = "contrib"

projects[uuid][type] = "module"
projects[uuid][version] = "1.0-alpha5"
projects[uuid][subdir] = "contrib"

projects[uuid_features][type] = "module"
projects[uuid_features][version] = "1.0-alpha4"
projects[uuid_features][subdir] = "contrib"


; STRUCTURE:

projects[blockify][type] = "module"
projects[blockify][version] = "1.2"
projects[blockify][subdir] = "contrib"

projects[context][type] = "module"
projects[context][version] = "3.2"
projects[context][subdir] = "contrib"

projects[context_accordion][type] = "module"
projects[context_accordion][version] = "1.0"
projects[context_accordion][subdir] = "contrib"

projects[context_condition_admin_theme][type] = "module"
projects[context_condition_admin_theme][version] = "1.0-beta1"
projects[context_condition_admin_theme][subdir] = "contrib"

; bug:  Fixed parent item is not working in Panels Pane
; todo: Create patch
projects[context_menu_block][type] = "module"
projects[context_menu_block][version] = "3.x-dev"
projects[context_menu_block][subdir] = "contrib"

projects[draggableviews][type] = "module"
projects[draggableviews][version] = "2.0"
projects[draggableviews][subdir] = "contrib"

projects[ds][type] = "module"
projects[ds][version] = "2.6"
projects[ds][subdir] = "contrib"

projects[eva][version] = 1.2
projects[eva][subdir] = "contrib"

projects[menu_block][type] = "module"
projects[menu_block][version] = "2.4"
projects[menu_block][subdir] = "contrib"

projects[menu_firstchild][type] = "module"
projects[menu_firstchild][version] = "1.x-dev"
projects[menu_firstchild][subdir] = "contrib"

projects[menu_attributes][type] = "module"
projects[menu_attributes][version] = "1.0-rc2"
projects[menu_attributes][subdir] = "contrib"

projects[power_menu][type] = "module"
projects[power_menu][version] = "2.x-dev"
projects[power_menu][subdir] = "contrib"

projects[semanticviews][type] = "module"
projects[semanticviews][version] = "1.0-alpha1"
projects[semanticviews][subdir] = "contrib"

projects[special_menu_items][type] = "module"
projects[special_menu_items][version] = "2.0"
projects[special_menu_items][subdir] = "contrib"

projects[taxonomy_menu][type] = "module"
projects[taxonomy_menu][version] = "1.5"
projects[taxonomy_menu][subdir] = "contrib"

projects[views][type] = "module"
projects[views][version] = "3.8"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][version] = "3.2"
projects[views_bulk_operations][subdir] = "contrib"

projects[views_megarow][version] = 1.3
projects[views_megarow][subdir] = "contrib"

projects[views_slideshow][type] = "module"
projects[views_slideshow][version] = "3.1"
projects[views_slideshow][subdir] = "contrib"

projects[views_tree][type] = "module"
projects[views_tree][version] = "2.x-dev"
projects[views_tree][subdir] = "contrib"


; TRANSLATE:

; todo: create custom patch
projects[dnl][type] = "module"
projects[dnl][version] = "1.4"
projects[dnl][subdir] = "contrib"

projects[entity_translation][type] = "module"
projects[entity_translation][version] = "1.x-dev"
projects[entity_translation][subdir] = "contrib"

projects[features_translations][type] = "module"
projects[features_translations][version] = "1.0-beta4"
projects[features_translations][subdir] = "contrib"

projects[i18n][type] = "module"
projects[i18n][version] = "1.x-dev"
projects[i18n][subdir] = "contrib"
	;projects[i18n][patch][] = "https://bitbucket.org/dinamico/contento/downloads/2227523.2.patch"

projects[i18n_menu_overview][type] = "module"
projects[i18n_menu_overview][version] = "1.x-dev"
projects[i18n_menu_overview][subdir] = "contrib"

projects[l10n_update][type] = "module"
projects[l10n_update][version] = "2.x-dev"
projects[l10n_update][subdir] = "contrib"

projects[multilink][type] = "module"
projects[multilink][version] = "2.9"
projects[multilink][subdir] = "contrib"

projects[translation_overview][type] = "module"
projects[translation_overview][version] = "2.x-dev"
projects[translation_overview][subdir] = "contrib"

projects[translation_table][type] = "module"
projects[translation_table][version] = "1.x-dev"
projects[translation_table][subdir] = "contrib"

projects[weight_language][type] = "module"
projects[weight_language][subdir] = "contrib"
projects[weight_language][download][type] = "git"
projects[weight_language][download][url] = "http://git.drupal.org/sandbox/jm.federico/1876362.git"
projects[weight_language][download][branch] = "7.x-1.x"
	;;projects[weight_language][download][revision] = 29ba320bb292274080559d4fd2939998c94cbcd9


; LIBRARIES:

libraries[backbone][download][type] = "file"
libraries[backbone][type] = "libraries"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/1.1.0.tar.gz"

	;libraries[bxslider][download][type] = "file"
	;libraries[bxslider][type] = "libraries"
	;libraries[bxslider][download][url] = "http://bxslider.com/lib/jquery.bxslider.zip"

  ;libraries[chosen][type] = "libraries"
  ;libraries[chosen][download][type] = "file"
  ;libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/archive/v0.9.15.zip"
  ;libraries[chosen][overwrite] = TRUE

  ;libraries[cloud-zoom][type] = "libraries"
  ;libraries[cloud-zoom][download][type] = "file"
  ;libraries[cloud-zoom][download][url] = "http://www.professorcloud.com/downloads/cloud-zoom.1.0.3.zip"

  ;libraries[jquery_ui_spinner][type] = "libraries"
  ;libraries[jquery_ui_spinner][download][type] = "file"
  ;libraries[jquery_ui_spinner][download][url] = "https://github.com/btburnett3/jquery.ui.spinner/archive/master.zip"

libraries[ckeditor][download][type] = get
libraries[ckeditor][type] = "libraries"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor%20for%20Drupal/edit/ckeditor_4.3.2_edit.zip"

libraries[colorbox][download][type] = "file"
libraries[colorbox][type] = "libraries"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"

	;libraries[flexslider][download][type] = "file"
	;libraries[flexslider][type] = "libraries"
	;libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/version/2.2.2.zip"

libraries[jquery.bxslider][type] = "libraries"
libraries[jquery.bxslider][download][type] = "file"
libraries[jquery.bxslider][download][url] = "https://github.com/wandoledzep/bxslider-4/archive/master.zip"

libraries[jquery_expander][type] = "libraries"
libraries[jquery_expander][download][type] = "file"
libraries[jquery_expander][download][url] = "https://github.com/kswedberg/jquery-expander/archive/master.zip"

  ;libraries[ie7-js][type] = "libraries"
  ;libraries[ie7-js][download][type] = "file"
  ;libraries[ie7-js][download][url] = "https://ie7-js.googlecode.com/files/ie7-2.1%28beta4%29.zip"

libraries[modernizr][download][type] = "file"
libraries[modernizr][type] = "libraries"
libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/v2.7.1.tar.gz"

libraries[plupload][download][type] = "file"
libraries[plupload][type] = "libraries"
libraries[plupload][download][url] = "https://github.com/moxiecode/plupload/archive/v1.5.8.zip"
libraries[plupload][patch][1903850] = "http://drupal.org/files/issues/plupload-1_5_8-rm_examples-1903850-16.patch"

libraries[timeago][download][type] = "file"
libraries[timeago][type] = "libraries"
libraries[timeago][download][url] = "https://raw.github.com/rmm5t/jquery-timeago/v1.3.1/jquery.timeago.js"

libraries[underscore][download][type] = "file"
libraries[underscore][type] = "libraries"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/1.5.2.zip"

libraries[imagesloaded][download][type] = "file"
libraries[imagesloaded][type] = "libraries"
libraries[imagesloaded][download][url] = "https://github.com/desandro/imagesloaded/archive/v2.1.2.tar.gz"

libraries[imgareaselect][download][type] = "file"
libraries[imgareaselect][type] = "libraries"
libraries[imgareaselect][download][url] = "http://odyniec.net/projects/imgareaselect/jquery.imgareaselect-0.9.10.zip"

  ;libraries[selectnav.js][type] = "libraries"
  ;libraries[selectnav.js][download][type] = "file"
  ;libraries[selectnav.js][download][url] = "https://github.com/lukaszfiszer/selectnav.js/archive/master.zip"


; THEMES:

projects[omega][type] = "theme"

projects[megaomega][type] = "theme"
projects[megaomega][download][type] = "file"
projects[megaomega][download][url] = "https://bitbucket.org/dinamico/megaomega/get/master.zip"

projects[seventhyseven][type] = "theme"
projects[seventhyseven][download][type] = "file"
projects[seventhyseven][download][url] = "https://bitbucket.org/dinamico/seventhyseven/get/master.zip"

